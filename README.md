# evoBOT Simulation Model

![evoBOT Model](docs/images/evobot_sim.png)

evoBOT from Fraunhofer Institute for Material Flow and Logistics (IML) is a highly dynamic and autonomous mobile robot. It can transport objects with up to 40kg at a travel speed of up to 10m/s. The evoBOT Simulation Model tries to match the real vehicle as close as possible and offers similar dynamics and sensor data. The simulation approach can reduce development times. First, prototypes can be tested in digital reality before they are built. Second, hardware and software development can be decoupled in this way. Details about evoBOT itself, the simulation model, its usage, and potential use cases can be found in the [documentation](/docs/index.adoc).

## Setup 
The requirements and installation of the evoBOT simulation model are described in [chapter 2](/docs/02_requirements.adoc) and [chapter 3](/docs/03_installation.adoc) of the documentation.

## Usage
The usage of the evoBOT model by controlling it via ROS or Isaac Gym is described in [chapter 4](/docs/04_usage.adoc).

## License
See the license file in the top directory.

## Resources
- GTC Spring 2023: [Training Highly Dynamic Robots for Complex Tasks in Industrial Applications](https://www.nvidia.com/gtc/session-catalog/?tab.catalogallsessionstab=16566177511100015Kus&search=fraunhofer%20iml#/session/1666649170799001kgqe)
- GTC Spring 2022: [Towards a Digital Reality in Logistics Automation: Optimization of Sim-to-Real](https://www.nvidia.com/en-us/on-demand/session/gtcspring22-s42559/)
- IEEE-RAM Publication: [Guided Reinforcement Learning: A Review and Evaluation for Efficient and Effective Real-World Robotics](https://ieeexplore.ieee.org/abstract/document/9926159)

## Contact information

Maintainer:
- Julian Eßer <a href="mailto:julian.esser@iml.fraunhofer.de?">julian.esser@iml.fraunhofer.de</a>

Development Team:
- Frido Feldmeier <a href="mailto:frido.feldmeier@iml.fraunhofer.de?">frido.feldmeier@iml.fraunhofer.de</a>
- Renato Gasoto <a href="mailto:rgasoto@nvidia.com?">rgasoto@nvidia.com</a>
